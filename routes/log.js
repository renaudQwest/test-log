var express = require('express');
var router = express.Router();

var res = require('express').response;
var req = require('express').request;

/* GET home page. */
router.get('/', function(req, res, next) {
   res.status(200)
   res.json({
       code: 200,
       msg: "Server working"
   });
   return res;
});

router.post('/', function(req, res, next) {
    let requestBody = req.body;

    if (!req.body.id || req.body.id === "" || req.body.id.length <= 3) {
        return res.status(422).json({ code: 422, msg: "Id is empty, not exists or contains less 3 characters"});
    }

    if (!req.body.file || req.body.file === "" || req.body.file.length <= 3) {
        return res.status(422).json({ code: 422, msg: "File is empty, not exists or contains less 3 characters"});
    }

    if (!req.body.date || req.body.date === "" || req.body.date.length <= 3) {
        return res.status(422).json({ code: 422, msg: "Date is empty, not exists or contains less 3 characters"});
    }

    const regex = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d+Z|\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z|\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}|\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}-\d{2}:\d{2}/

    if (!req.body.date.match(regex)) {
        return res.status(402).json({code: 422, msg: {
            error: "Date format is wrong ! Please correct your date with YYYY-MM-DDTHH:MM:SS+HH:MM",
            sample: new Date().toISOString()
            }
        })
    }

    if (!req.body.status || req.body.status === "" || req.body.status.length <= 5) {
        return res.status(422).json({ code: 422, msg: "Status is empty, not exists or contains less 5 characters"});
    }

    let responseBody = {
        code: 200,
        msg: "Ok",
        data: requestBody,
    };

    console.log(responseBody);

    return res.status(200).json(responseBody);
});

module.exports = router;
