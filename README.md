# Installation 

```BASH
git clone https://gitlab.com/renaudQwest/test-log.git
```

# Dependencies

with Yarn
```BASH
yarn
```

with npm 
```BASH
npm i
```

or 

```BASH
npm install
```

# Launching

with Yarn
```BASH
yarn dev
```

with npm
```BASH
npm dev
```
is launched on localhost:3000/

# Working on
Send a POST at localhost:3000/log 

Happy hacking
